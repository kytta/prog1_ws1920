/**
 * Ein Programm, das die Pflichtaufgabe des Übungsblattes 02 löst !
 *
 * @author Nikita Karamov
 * @author Nicolas Schubert
 */
public class MinecraftWasser {
    public static void main(String[] args) {
        int[] landschaft = buildLandschaft(args);
        /*
         * Die optische Ausgabe der Landschaft (ursprüngliche und gefüllte Landschaft)
         */
        System.out.println(fancyPrint("Die ursprüngliche Landschaft:", true, true, "32"));
        printLadschaft(landschaft);

        System.out.println(fancyPrint("\nDie “gefüllte” Landschaft:", true, true, "34"));
        int[] wasser = calculateWater(landschaft);
        printLadschaft(landschaft, wasser);
    }

    /**
     * Formatiert ein String mithilfe der ANSI-Escapesequenzen
     *
     * @param text      Text, der formatiert werden soll
     * @param underline Text unterstrichen
     * @param bold      Text fett machen
     * @param colorCode ANSI-Escapesequenz für die Farbe (30-37, 90-97)
     * @return String mit ANSI-Escapesequenzen fürs Ausdrucken
     */
    public static String fancyPrint(String text, boolean underline, boolean bold, String colorCode) {
        StringBuilder message = new StringBuilder();
        if (underline) {
            message.append("\u001B[4m");
        }
        if (bold) {
            message.append("\u001B[1m");
        }
        if (!colorCode.equals("")) {
            message.append("\u001B[").append(colorCode).append("m");
        }
        message.append(text);
        message.append("\u001B[0m");
        return message.toString();
    }

    /**
     * Findet das größte Element im Array (im Bezug auf die Parameter)
     *
     * @param array Array, der durchgesucht wird
     * @return das größte Element des Arrays
     */
    public static int findMax(int[] array) {
        int max = array[0];

        for (int element : array) {
            if (element > max) {
                max = element;
            }
        }

        return max;
    }

    /**
     * Liegt ein Array mit Landschaftsdaten auf der Basis von Argumenten.
     *
     * Ergibt ein Array, wo jedes Element die Höhe der Landschaft zeigt.
     *
     * @param args die Kommandozeilen Parameter
     * @return ein Array mit Landschaftsdaten
     */
    public static int[] buildLandschaft(String[] args) {
        /* === Anlegen eines Arrays fur die Landschaft === */

        int n = 0; // Größe der Landschaft
        try {
            n = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println(fancyPrint("Der erste Parameter ist keine Nummer.", false, true, "31"));
            System.exit(1);
        }

        if (args.length != n + 1) {
            System.out.println(fancyPrint("Die Anzahl der Parameter ist falsch.", false, true, "31"));
            System.exit(2);
        }

        int[] landschaft = new int[n];

        /* === Füllen des Arrays mit der Beschreibung der Landschaft === */
        for (int i = 1; i < n + 1; i++) {
            try {
                landschaft[i - 1] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println(fancyPrint("Alle Parameter sollen Nummern sein", false, true, "31"));
                System.exit(1);
            }
        }

        return landschaft;
    }

    /**
     * Berechnet die Höhe des Wassersniveaus für gegebene Landschaftsdaten
     *
     * Alle freien Felder, die sowohl links als auch rechts von sich einen Block
     * stehen haben, lassen sich mit Wasser füllen (Blöcke links und rechts können,
     * müssen keine direkten Nachbarn sein).
     *
     * Ergibt ein Array, wo jedes Element die Höhe des Wassersniveaus zeigt.
     *
     * @param landschaft ein Array mit Landschaftsdaten
     * @return ein Array mit Wasserdaten
     */
    public static int[] calculateWater(int[] landschaft) {
        int[] result = new int[landschaft.length];
        int maxLandschaft = findMax(landschaft);

        for (int i = 0; i < landschaft.length; i++) {
            for (int j = maxLandschaft; j > 0; j--) {
                if (landschaft[i] >= j) {
                    break;
                }

                boolean left = false;
                boolean right = false;

                for (int toLeft = i - 1; toLeft >= 0; toLeft--) {
                    if (landschaft[toLeft] >= j) {
                        left = true;
                        break;
                    }
                }
                for (int toRight = i + 1; toRight < landschaft.length; toRight++) {
                    if (landschaft[toRight] >= j) {
                        right = true;
                        break;
                    }
                }

                if (left && right) {
                    result[i] = j;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Druckt die Landschaft ohne Wasser basierend auf den Landschaftsdaten
     *
     * @param landschaft Landschaftsdaten
     */
    public static void printLadschaft(int[] landschaft) {
        // Als Wasser wird ein leeres Array angegeben, bzw. [0, ..., 0]
        printLadschaft(landschaft, new int[landschaft.length]);
    }

    /**
     * Druckt die Landschaft mit Wasser basierend auf die Landschaftsdaten und die
     * Wasserdaten
     *
     * @param landschaft Landschaftsdaten
     * @param water      Wasserdaten
     */
    public static void printLadschaft(int[] landschaft, int[] water) {
        int maxLandschaft = findMax(landschaft);

        /*
         * Um die Landschaft anzuzeigen, gehen wir von oben links nach unten rechts
         * gehen, weil die Konsole so "funktioniert".
         */
        for (int i = maxLandschaft; i > 0; i--) {
            // für jede Ebene
            StringBuilder ebene = new StringBuilder();
            // weil die Strings in Java nicht optimiert sind

            /*
             * Für jede Ebene, die wir anzeigen möchten, berechnen wir, ob die Spalte, die
             * wir zurzeit anzeigen, höheres Niveau hat, als die Ebene, oder nicht.
             */
            for (int j = 0; j < landschaft.length; j++) {
                /*
                 * Für jede "Spalte", wenn das Niveau unseres "Anzeigers" (i) niedriger ist, als
                 * die Höhe der Spalte, zeichnen wir "[X]" (= Erde), sonst zeichnen wir drei
                 * Leerzeichen (= Luft) Falls Wasser angegeben ist, nutzen wir nicht drei
                 * Leerzeichen (=Luft) sondern drei Mal ~~~ (=Wasser) unter Berücksichtung der
                 * Bedingungen wann Wasser zum Einsatz kommt
                 */
                if (i <= water[j] && i > landschaft[j]) {
                    ebene.append(fancyPrint("~~~", false, false, "36"));
                } else if (i <= landschaft[j]) {
                    ebene.append("[X]"); // sieht schöner aus als nur "X"
                } else {
                    ebene.append("   "); // soll auch drei Zeichen lang sein
                }
            }

            System.out.println(ebene.toString());
        }
    }
}
