/**
 * Ein Programm, das die Pflichtaufgabe des Übungsblattes 03 löst !
 *
 * @author Nikita Karamov
 * @author Nicolas Schubert
 */
import java.util.Scanner;

public class TicTacToe {
    /* SPIEL-EINSTELLUNGEN - Der Player und des Spielfelds*/
    private static final char[] PLAYERS = {'X', 'O'};
    private static final int FIELD_SIZE = 5;

    /* SPIEL-DATEN */
    private static char[] players = PLAYERS; // die Spieler
    private static int currentPlayer = 0; // index des derzeitiges Spielers
    private static char[][] field = new char[FIELD_SIZE][FIELD_SIZE]; // das Feld

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (emptyFieldsPresent() && whoWon() == '\0') {
            displayField();

						/* optische Ausgabe der Aufforderung an den Spieler*/
            System.out.println(String.format("Am Zug: Spieler %c", players[currentPlayer]));
            System.out.println(String.format("Bitte die Koordinate für Reihe und Spalte (mit Leerzeichen geteilt) eingeben (0-%d): ", field.length - 1));

            String rowString, colString;
            do {
                rowString = sc.next();
                colString = sc.next();
            } while (!coordinatesCorrect(rowString, colString));
            int rowIndex = Integer.parseInt(rowString);
            int colIndex = Integer.parseInt(colString);

            field[rowIndex][colIndex] = PLAYERS[currentPlayer];

            currentPlayer++;
            if (currentPlayer >= players.length) {
                currentPlayer = 0;
            }

            System.out.println("\n\n");
        }
        sc.close();

        char winner = whoWon();

        displayField();

				/*
				Falls ein Spieler gewinnt kommt eine Ausgabe - falls keiner
				gewinnen sollte die Ausgabe für ein Unentschieden - wenn kein
				Gewinner oder Unentschieden erreicht wurde, dann ist etwas
				fehlgeschlafen
				*/
        if (winner != '\0') {
            System.out.println(String.format("Spieler %c gewinnt!", winner));
        } else if (!emptyFieldsPresent()) {
            System.out.println("Unentschieden.");
        } else {
            System.err.println("Etwas ist schief gegangen...");
            System.exit(1);
        }
    }

    /**
     * Gibt eine waagerechte Linie aus Minuszeichen aus
     */
    private static void printLine() {
        printLine(false);
    }

    /**
     * Gibt eine waagerechte Linie mit Indexen aus Minuszeichen aus
     */
    private static void printLine(boolean withIndices) {
        for (int i = 0; i < field.length * 4 + 1; i++) {
            if (withIndices) {
                if ((i - 2) % 4 == 0) {
                    System.out.print((i - 2) / 4);
                    continue;
                }
            }
            System.out.print('—');
        }
        System.out.println();
    }
		/*
		Ausgabe Falls die eingegebenen Zahlen nicht den Bedingungene
		entsprechen
		*/
    private static boolean coordinatesCorrect(String rowString, String colString) {
        int rowIndex, colIndex;
        try {
            rowIndex = Integer.parseInt(rowString);
            colIndex = Integer.parseInt(colString);
        } catch (NumberFormatException e) {
            System.out.println("Das scheint keine Nummer zu sein.");
            return false;
        }

        if (rowIndex >= field.length || colIndex >= field.length || rowIndex < 0 || colIndex < 0) {
            System.out.println("Falsche Angabe. Nummern zu groß u./o. zu klein.");
            return false;
        }

        if (field[rowIndex][colIndex] != '\0') {
            System.out.println("Feld schon bezetzt.");
            return false;
        }

        return true;
    }

    /**
     * Gibt das aktuelle Spielfeld aus
     */
    private static void displayField() {
        printLine(true);

        for (int i = 0; i < field.length; i++) {
            char[] row = field[i];
            System.out.print(i);
            for (char cell : row) {
                if (cell == '\0') {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + cell + " ");
                }
                System.out.print('|');
            }
            System.out.println();
            printLine();
        }
    }

    /**
     * Prüft eine Reihe, dass sie die gleichen Zeichen in allen Feldern hat
     *
     * @param rowIndex Zeilennummer
     * @return Zeichen, dass in allen Feldern der Reihe steht, sonst `null`-Zeichen
     */
    private static char checkRow(int rowIndex) {
        char winner = '\0';

        for (char cell : field[rowIndex]) {
            if (cell == '\0') {
                // falls mindestens ein Feld leer ist, ist die Reihe nicht voll
                winner = '\0';
                break;
            } else if (winner == '\0') {
                winner = cell;
            } else if (winner != cell) {
                // falls es unterschiedliche Zeichen in der Reihe gibt,
                // hat keiner gewonnen (für die Reihe)
                winner = '\0';
                break;
            }
        }

        return winner;
    }

    /**
     * Prüft eine Spalte, dass sie die gleichen Zeichen in allen Feldern hat
     *
     * @param colIndex Spaltennummer
     * @return Zeichen, dass in allen Feldern der Spalte steht, sonst `null`-Zeichen
     */
    private static char checkColumn(int colIndex) {
        char winner = '\0';

        for (char[] row : field) {
            if (row[colIndex] == '\0') {
                winner = '\0';
                break;
            } else if (winner == '\0') {
                winner = row[colIndex];
            } else if (winner != row[colIndex]) {
                // falls mindestens ein Feld leer ist
                // ODER es unterschiedliche Zeichen in der Spalte gibt,
                // hat keiner gewonnen (für die Spalte)
                winner = '\0';
                break;
            }

        }

        return winner;
    }

    /**
     * Prüft eine Diagonale, dass sie die gleichen Zeichen in allen Feldern hat
     *
     * @param main ob die DIagonale eine Haupt-Diagonale ist o. nicht
     * @return Zeichen, dass in allen Feldern der Diagonale steht, sonst `null`-Zeichen
     */
    private static char checkDiagonal(boolean main) {
        char winner = '\0';

        for (int rowIndex = 0; rowIndex < field.length; rowIndex++) {
            int colIndex = main ? rowIndex : field[rowIndex].length - 1 - rowIndex;

            if (field[rowIndex][colIndex] == '\0') {
                // falls mindestens ein Feld leer ist,
                // hat keiner gewonnen (für die Diagonale)
                winner = '\0';
                break;
            } else if (winner == '\0') {
                winner = field[rowIndex][colIndex];
            } else if (winner != field[rowIndex][colIndex]) {
                // falls es unterschiedliche Zeichen in der Diagonale gibt,
                // hat keiner gewonnen (für die Diagonale)
                winner = '\0';
                break;
            }
        }

        return winner;
    }

    /**
     * Prüft das ganze Feld, ob jemand gewonnen hat.
     *
     * @return Zeichen des Spielers, der gewonnen hat, oder `null` falls noch
     * niemand gewinnt.
     */
    private static char whoWon() {
        char winner = '\0';

        // wir sollen alle Zeilen,
        // alle Spalten und beide
        // Diagonalen prüfen
        int toCheck = field.length + field.length + 2;

        int checking = 1;

        while (winner == '\0' && checking <= toCheck) {
            if (checking > 2 * field.length) {
                // Diagonalen
                winner = checkDiagonal(checking == field.length * 2 + 1);
            } else if (checking > field.length) {
                // Spalten
                winner = checkColumn(checking - field.length - 1);
            } else {
                // Zeilen
                winner = checkRow(checking - 1);
            }

            checking++;
        }

        return winner;
    }

    /**
     * Prüft, ob es noch leere Plätze gibt
     *
     * @return true falls mind. ein leerer Platz existiert, sonst false
     */
    private static boolean emptyFieldsPresent() {
        for (char[] row : field) {
            for (char cell : row) {
                if (cell == '\0') {
                    return true;
                }
            }
        }

        return false;
    }
}
