package minesweeper;

import java.util.Scanner;

import minesweeper.action.Action;
import minesweeper.board.Board;
import minesweeper.coordinates.Coordinates;
import minesweeper.input.ActionInputProcessor;
import minesweeper.input.CoordinatesInputProcessor;

/**
 * Gesamtes Spielverlauf
 */
public class Game {
    private static final int SIZE = 10;
    private static final int MINES = 23;
    private static final int OPEN = 5;
    private static final ActionInputProcessor ACTION_IP = new ActionInputProcessor();
    private static final CoordinatesInputProcessor COORDS_IP = new CoordinatesInputProcessor();
    private static final Scanner scanner = new Scanner(System.in);

    private static final Board BOARD = new Board(SIZE, MINES, OPEN);

    public static void main(String[] args) {
        System.out.println("Minesweeper!");

        while (BOARD.isAlive() && BOARD.fieldsLeft() > 0) {
            System.out.println(BOARD.toString());

            Coordinates coordinates = COORDS_IP.read(scanner, BOARD);
            Action action = ACTION_IP.read(scanner, null);

            BOARD.performAction(action, coordinates);
        }
        
        if (BOARD.isAlive()) {
            System.out.println("Gratulation! Du hast gewonnen!");
        } else {
            System.out.println("Wie schade!");
        }
    }
}
