package minesweeper.action;

import minesweeper.field.Field;

/**
 * Interface, der eine Aktion beschreibt, die auf einem Feld ausgeführt werden kann
 */
public interface Action {
    /**
     * Ausführt die Aktion
     *
     * @param field Feld
     * @return ob das Feld nach der Ausführung noch "lebt" (i.e. nicht explodiert wurde)
     */
    boolean execute(Field field);
}