package minesweeper.action;

import minesweeper.field.Field;

/**
 * Entschärfen-Aktion
 */
public class DefuseAction implements Action {
    /**
     * Ausführt die Entschärfen-Aktion
     *
     * @param field Feld
     * @return `true`, weil nach der Entschärfung kann das Feld nicht "tot" (i.e. explodiert) sein
     */
    @Override
    public boolean execute(Field field) {
        field.defuse();
        return true;
    }
}