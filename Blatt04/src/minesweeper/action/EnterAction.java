package minesweeper.action;

import minesweeper.field.Field;

/**
 * Betreten-Aktion
 */
public class EnterAction implements Action {
    /**
     * Ausführt die Betreten-Aktion
     *
     * @param field Feld
     * @return ob das Feld nach dem Betreten noch "lebt" (i.e. nicht explodiert wurde)
     */
    @Override
    public boolean execute(Field field) {
        field.enter();
        return !field.isBlownUp();
    }
}