package minesweeper.board;

import java.util.Collections;
import java.util.Stack;

import minesweeper.action.Action;
import minesweeper.coordinates.Alphabet;
import minesweeper.coordinates.Coordinates;
import minesweeper.field.Field;
import minesweeper.field.LandField;
import minesweeper.field.MineField;

/**
 * Spielfeld-Klasse
 */
public class Board {

    private Field[][] fields;
    private int unopenedFieldsLeft;
    private boolean lifeStatus = true;

    /**
     * Initialisiert ein Spielfeld und generiert alle Felder dafür
     *
     * @param size       Größe des Spielfeldes
     * @param mines      Anzahl der Minen auf dem Spielfeld
     * @param openFields Anzahl der geöffneten Felder
     * @throws IllegalArgumentException wenn die obengenannte Parameter falsch sind
     */
    public Board(int size, int mines, int openFields) throws IllegalArgumentException {
        if (size > Alphabet.FULL.length()) {
            throw new IllegalArgumentException("Spielfeldgröße zu groß.");
        }

        if (mines + openFields >= size * size) {
            throw new IllegalArgumentException("Nicht möglich, so viele Felder auf Spielfeld so klein zu platzieren.");
        }

        fields = new Field[size][size];

        Stack<Coordinates> possibleCoordinates = getShuffledCoordinates(fields);

        for (int i = 0; i < mines; i++) {
            Coordinates coordinates = possibleCoordinates.pop();
            int row = coordinates.getRow();
            int col = coordinates.getCol();

            fields[row][col] = new MineField();
        }
        for (int i = 0; i < openFields; i++) {
            Coordinates coordinates = possibleCoordinates.pop();
            int row = coordinates.getRow();
            int col = coordinates.getCol();

            fields[row][col] = new LandField(true, calculateValue(coordinates));
        }

        this.unopenedFieldsLeft = possibleCoordinates.size();

        while (possibleCoordinates.size() > 0) {
            Coordinates coordinates = possibleCoordinates.pop();
            int row = coordinates.getRow();
            int col = coordinates.getCol();

            fields[row][col] = new LandField(false, calculateValue(coordinates));
        }
    }

    /**
     * Gibt ein Feld aus dem Spielfeld zurück
     *
     * @param coordinates Koordinaten
     * @return ein Feld, falls es existiert, sonst `null`
     */
    public Field getField(Coordinates coordinates) {
        int row = coordinates.getRow();
        int col = coordinates.getCol();
        if (row >= 0 && col >= 0 && row <= fields.length && col <= fields[row].length) {
            return fields[row][col];
        }
        return null;
    }

    /**
     * Zeigt, ob der Spieler noch am Leben ist
     *
     * @return `true` wenn am Leben, sonst `false`
     */
    public boolean isAlive() {
        return lifeStatus;
    }

    /**
     * Gibt die Anzahl der nicht erkundenen Felder
     *
     * @return die Anzahl
     */
    public int fieldsLeft() {
        return unopenedFieldsLeft;
    }

    /**
     * Ausführt eine Aktion mit einem Feld unter bestimmten Koordinaten
     *
     * @param action      Aktion
     * @param coordinates Koordinaten des Feldes
     */
    public void performAction(Action action, Coordinates coordinates) {
        this.lifeStatus &= action.execute(getField(coordinates));
        this.unopenedFieldsLeft -= 1;
    }

    ////////////////////////////
    //      FELD-AUSGABE      //
    ////////////////////////////

    /**
     * Gibt die Feld-Daten als eine String zurück
     *
     * @return Feld
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        addLine(sb, true);

        for (int i = 0; i < fields.length; i++) {
            Field[] row = fields[i];
            sb.append('|');
            for (Field field : row) {
                sb.append(' ').append(field.toChar()).append(' ');
                sb.append('|');
            }
            sb.append("  ").append(i).append('\n');
            addLine(sb, false);
        }

        return sb.toString();
    }

    /**
     * Fügt eine Linie aus '–'-Zeichen zu einem StringBuilder
     *
     * @param sb          StringBuilder
     * @param withIndices ob die Linie die Indizen der Kacheln enthalten muss
     */
    private void addLine(StringBuilder sb, boolean withIndices) {
        for (int i = 0; i < fields.length * 4 + 1; i++) {
            if (withIndices && (i - 2) % 4 == 0) {
                sb.append(Alphabet.intToChar((i - 2) / 4));
            } else {
                sb.append('–');
            }
        }
        sb.append('\n');
    }

    /**
     * Generiert eine Liste von gemischten Koordinaten
     *
     * @param fields Felder-Array, für denen die Koordinaten generiert werden sollen
     * @return Liste mit den Koordinaten
     */
    private Stack<Coordinates> getShuffledCoordinates(Field[][] fields) {
        Stack<Coordinates> coordinates = new Stack<>();
        for (int rowNum = 0; rowNum < fields.length; rowNum++) {
            for (int colNum = 0; colNum < fields[rowNum].length; colNum++) {
                coordinates.add(new Coordinates(rowNum, colNum));
            }
        }
        Collections.shuffle(coordinates);
        return coordinates;
    }


    /**
     * Findet die Zahl der zu einem Feld adjazenten Minen
     *
     * @param coordinates Koordinaten
     * @return die Zahl der Minen als ein `char`
     */
    private char calculateValue(Coordinates coordinates) {
        int row = coordinates.getRow();
        int col = coordinates.getCol();
        byte sum = 0;

        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i >= 0 && j >= 0 && i < fields.length && j < fields[i].length && i != j) {
                    if (fields[i][j] instanceof MineField) {
                        sum++;
                    }
                }
            }
        }

        return Character.forDigit(sum, 10);
    }
}
