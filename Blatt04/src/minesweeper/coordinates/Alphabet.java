package minesweeper.coordinates;

/**
 * Dienst-Klasse für Umwandeln der Zeichen und Nummern hin und zurück
 */
public class Alphabet {
    public static final String FULL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Alphabet() {
    }

    /**
     * Umwandelt ein Nummer in eine Buchstabe gemäß des Alphabets
     * @param number Nummer
     * @return Buchstabe
     *
     * @throws IllegalArgumentException wenn der Nummer zu groß oder zu klein ist
     */
    public static char intToChar(int number) {
        if (number < FULL.length() && number >= 0) {
            return FULL.charAt(number);
        } else {
            throw new IllegalArgumentException(
                    String.format("Nummer nicht im Bereich: %d–%d", 0, FULL.length() - 1));
        }
    }

    /**
     * Umwandelt eine Buchstabe in einen Nummer gemäß des Alphabets
     * @param letter Buchstabe
     * @return Nummer
     *
     * @throws IllegalArgumentException wenn die Buchstabe im Alphabet nicht vorhanden ist
     */
    public static int charToInt(char letter) throws IllegalArgumentException {
        char upperChar = Character.toUpperCase(letter);
        int indexOf = FULL.indexOf(upperChar);

        if (indexOf != -1) {
            return indexOf;
        } else {
            throw new IllegalArgumentException(
                    String.format("Zeichen %c nicht im Alphabet vorhanden.", upperChar));
        }

    }
}
