package minesweeper.coordinates;

/**
 * Daten-Klasse für Koordinaten
 */
public class Coordinates {
    private final int row;
    private final int col;

    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Coordinates(int row, char col) {
        this.row = row;
        this.col = Alphabet.charToInt(col);
    }

    public int getRow() {
        return this.row;
    }

    public int getCol() {
        return this.col;
    }
}
