package minesweeper.field;


/**
 * Ein Feld
 */
public abstract class Field {
    protected boolean open;
    protected char value;

    public Field(boolean open, char value) {
        this.open = open;
        this.value = value;
    }

    /**
     * Betreten eines Feldes
     */
    public abstract void enter();

    /**
     * Entschärfen eines Feldes
     */
    public abstract void defuse();

    /**
     * Zeigt ob das Feld explodiert ist
     *
     * @return `false` falls das Feld explodiert wurde, sonst `true`
     */
    public abstract boolean isBlownUp();

    /**
     * Öffnet das Feld, damit es sichtbar wird
     */
    protected final void open() {
        if (this.open) {
            System.out.println("Feld bereits betreten");
            return;
        }
        this.open = true;
    }

    /**
     * Gibt das Zeichen, das im Feld angezeigt werden soll
     *
     * @return Wert-Zeichen des Feldes falls es offen ist, sonst ein Leerzeichen
     */
    public final char toChar() {
        return this.open ? this.value : ' ';
    }
}
