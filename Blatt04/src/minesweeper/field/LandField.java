package minesweeper.field;

/**
 * Einfaches Feld
 */
public class LandField extends Field {
    public LandField(boolean open, char value) {
        super(open, value);
    }

    @Override
    public void enter() {
        System.out.println("Feld sicher.");
        this.open();
    }

    @Override
    public void defuse() {
        System.out.println("Keine Mine erwischt.");
        this.enter();
    }

    /**
     * Zeigt ob das Feld noch "lebt" (i.e. nicht explodiert ist)
     *
     * @return immer `false`, weil ein einfaches Feld nicht explodieren kann
     */
    @Override
    public boolean isBlownUp() {
        return false;
    }
}
