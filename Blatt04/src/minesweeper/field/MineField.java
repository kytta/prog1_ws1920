package minesweeper.field;

/**
 * Vermintes Feld
 */
public class MineField extends Field {
    private boolean defused;

    public MineField() {
        super(false, '*');
        this.defused = false;
    }

    @Override
    public void enter() {
        if (!this.defused) {
            System.out.println("Du bist explodiert. Ruhe in Frieden.");
        }
        this.open();
    }

    @Override
    public void defuse() {
        System.out.println("Mine erwischt und entschärft.");
        this.defused = true;
        this.value = '!';
        this.enter();
    }

    @Override
    public boolean isBlownUp() {
        return this.open && !this.defused;
    }
}
