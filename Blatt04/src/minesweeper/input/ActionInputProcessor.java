package minesweeper.input;

import java.util.InputMismatchException;
import java.util.Scanner;

import minesweeper.action.Action;
import minesweeper.action.DefuseAction;
import minesweeper.action.EnterAction;

public class ActionInputProcessor extends InputProcessor<Action, Object> {

    public ActionInputProcessor() {
        super("Aktion wählen: 0 für 'betreten', 1 für 'entschärfen': ");
    }

    /**
     * Lest die Eingabe des Nutzers bezüglich der Aktion und validiert die
     *
     * @param scanner Scanner
     * @param vC      nicht benötigt; einfach `null` passt
     * @return Aktion
     */
    @Override
    protected Action readAndValidate(Scanner scanner, Object vC) {
        try {
            int actionCode = scanner.nextInt();

            switch (actionCode) {
                case 0:
                    return new EnterAction();
                case 1:
                    return new DefuseAction();
                default:
                    System.out.println("Bitte entweder 0 o. 1 eingeben.");
                    return this.readAndValidate(scanner, vC);
            }
        } catch (InputMismatchException e) {
            System.out.println("Eingabe ist keine Zahl. Bitte erneut versuchen.");
            return this.readAndValidate(scanner, vC);
        }
    }
}
