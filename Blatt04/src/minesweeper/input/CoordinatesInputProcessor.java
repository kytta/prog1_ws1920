package minesweeper.input;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

import minesweeper.board.Board;
import minesweeper.coordinates.Coordinates;

public class CoordinatesInputProcessor extends InputProcessor<Coordinates, Board> {

    public CoordinatesInputProcessor() {
        super("Feld-Koordinaten eingeben (z.B. \"H4\"): ");
    }

    /**
     * Lest die Eingabe des Nutzers bezüglich der Koordinaten und validiert die
     *
     * @param scanner Scanner
     * @param board   der Spielfeld
     * @return die Koordinaten
     */
    @Override
    protected Coordinates readAndValidate(Scanner scanner, Board board) {
        Pattern oldPattern = scanner.delimiter();
        try {
            scanner.useDelimiter("");

            char col = scanner.next().charAt(0);
            int row = scanner.nextInt();
            scanner.next();

            Coordinates inputCoordinates = new Coordinates(row, col);

            if (board.getField(inputCoordinates) == null) {
                System.out.println("Eingegebene Koordinaten existiren nicht am Spielfeld.");
                return this.readAndValidate(scanner, board);
            }

            return inputCoordinates;
        } catch (InputMismatchException e) {
            System.out.println("Eingabe ist falsch. Bitte erneut versuchen.");
            return this.readAndValidate(scanner, board);
        } finally {
            scanner.useDelimiter(oldPattern);
        }
    }
}
