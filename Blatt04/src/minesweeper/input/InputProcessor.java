package minesweeper.input;

import java.util.Scanner;

/**
 * Klasse, die die Eingabe des Nutzers verarbeitet
 *
 * @param <S> Klasse, die als Verarbeitungsergebnis ausgegeben sein soll
 * @param <V> Klasse, damit die Gültigkeit des Ergebnisses geprüft wird
 */
public abstract class InputProcessor<S, V> {

    private final String promptMessage;

    public InputProcessor(String promptMessage) {
        this.promptMessage = promptMessage;
    }

    protected abstract S readAndValidate(Scanner scanner, V validationContext);

    /**
     * Lest die Eingabe des Nutzers und validiert die
     *
     * @param scanner           Scanner
     * @param validationContext Kontext für Validierung
     * @return die verarbeitete Daten
     */
    public final S read(Scanner scanner, V validationContext) {
        System.out.println(this.promptMessage);
        return this.readAndValidate(scanner, validationContext);
    }
}
