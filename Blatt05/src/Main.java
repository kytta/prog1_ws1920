import set.IntSet;
import tree.Tree;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree(5);
        tree.insert(4);
        tree.insert(2);
        tree.insertIterative(3);

        System.out.printf("       Baum: %s%n%n", tree.toString());

        System.out.printf("     Höhe: %d%n", tree.height());
        System.out.printf("4 im Baum: %b%n", tree.exists(4));
        System.out.printf("6 im Baum: %b%n", tree.exists(6));
        System.out.printf("      Min: %d%n", tree.min());
        System.out.printf("      Max: %d%n", tree.max());
        System.out.printf(" Entartet: %b%n", tree.isDegenerate());
        System.out.printf("  Alle >3: %b%n", tree.forAll(v -> v.getRootValue() > 3));
        System.out.printf(" Alle <20: %b%n", tree.forAll(v -> v.getRootValue() < 20));

        System.out.println("\n\n");

        IntSet set1 = new IntSet();
        set1.insert(1);
        set1.insert(1);
        set1.insert(2);
        set1.insert(3);

        IntSet set2 = new IntSet();
        set2.insert(2);
        set2.insert(3);
        set2.insert(5);
        set2.insert(7);

        IntSet set3 = new IntSet();
        set3.insert(2);
        set3.insert(3);

        System.out.printf("      Set 1: %s%n", set1.toString());
        System.out.printf("      Set 2: %s%n", set2.toString());
        System.out.printf("      Set 3: %s%n%n", set3.toString());
        System.out.printf("    5 in S1: %b%n", set1.contains(5));
        System.out.printf("    5 in S2: %b%n", set2.contains(5));
        System.out.printf("      Union: %s%n", set1.union(set2).toString());
        System.out.printf("      Isect: %s%n", set1.intersection(set2).toString());
        System.out.printf("   S1 == S2: %b%n", set1.equals(set2));
        System.out.printf("S3 == isect: %b%n", set3.equals(set1.intersection(set2)));

    }
}
