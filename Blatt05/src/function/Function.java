package function;

/**
 * Funktionales Interface mit einer Prädikaten-Funktion, die auf einem Objekt ausgeführt wird
 */
public interface Function<T> {
    /**
     * Die Prädikaten-Funktion. Wird auf einem Objekt ausgeführt.
     *
     * @param object Objekt für Ausführung
     * @return `true` oder `false`
     */
    boolean predicate(T object);
}
