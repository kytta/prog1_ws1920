package set;

import tree.Tree;

import java.util.Iterator;

/**
 * Implementation einer Menge
 */
public class IntSet implements Iterable<Integer> {
    private final Tree items;

    public IntSet() {
        this.items = new Tree();
    }

    public IntSet(int[] items) {
        this.items = new Tree();

        for (int i : items) {
            this.items.insert(i);
        }
    }

    /**
     * Fügt einen neuen Wert rekursiv in die Menge ein.
     * <p>
     * Falls ein Wert schon in der Menge existiert, wird er nicht hinzugefügt.
     *
     * @param value - Wert, der eingefügt wird
     */
    public void insert(int value) {
        this.items.insert(value);
    }

    /**
     * Prüƒt, ob der eingegebene Wert in der Menge vorhanden ist
     *
     * @param value Wert, dessen Existenz geprüft wird
     * @return `true` falls Wert in der Menge vorhanden ist, sonst `false`
     */
    public boolean contains(int value) {
        return this.items.exists(value);
    }

    /**
     * Gibt die Vereinigung der Mengen aus
     *
     * @param other die andere Menge; mit der wird diese Menge vereinigt
     * @return die Vereinigung
     */
    public IntSet union(IntSet other) {
        IntSet result = new IntSet();

        for (Integer el : this) {
            result.insert(el);
        }
        for (Integer el : other) {
            result.insert(el);
        }

        return result;
    }

    /**
     * Gibt den Durchschnitt der Mengen aus
     *
     * @param other die andere Menge; mit der wird diese Menge durchgeschnitten
     * @return der Durchschnitt
     */
    public IntSet intersection(IntSet other) {
        IntSet result = new IntSet();

        for (Integer el : this) {
            if (other.contains(el)) {
                result.insert(el);
            }
        }

        return result;
    }

    /**
     * Prüft, ob diese Menge gleich anderer Menge ist
     *
     * @param object andere Menge (oder irgendwelcher Objekt)
     * @return `true` falls die Mengen gleich sind, sonst `false`
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        IntSet other = (IntSet) object;

        Iterator<Integer> thisIter = this.iterator();
        Iterator<Integer> otherIter = other.iterator();

        while (thisIter.hasNext() && otherIter.hasNext()) {
            if (!thisIter.next().equals(otherIter.next())) return false;
        }

        return !thisIter.hasNext() && !otherIter.hasNext();
    }

    /**
     * Gibt die String-Repräsentation der Menge aus
     *
     * @return Liste aller Werten in einer String-Repräsentation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");

        for (Integer el : this) {
            sb.append(el).append(' ');
        }

        sb.append('}');

        return sb.toString();
    }

    /**
     * Gibt den Iterator für die Werte im Baum aus
     * @return Iterator über {@link Integer}
     */
    @Override
    public Iterator<Integer> iterator() {
        return this.items.iterator();
    }
}