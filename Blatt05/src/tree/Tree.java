package tree;

import function.Function;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation eines binären Suchbaumes
 */
public class Tree implements Iterable<Integer> {
    private Integer value;
    private Tree lChild; // left child
    private Tree rChild; // right child

    public Tree() {
        this(null);
    }

    public Tree(Integer value) {
        this.value = value;

        if (value != null)
            makeLeaves();
    }

    /**
     * Erzeugt leere Blätter für den Baum.
     * <p>
     * Diese Methode wird nur einmal ausgeführt — wenn der Wert des Wurzels zum ersten Mal nicht `null` wird. Das wurde
     * dafür gemacht, dass die `insert`-Methode hübscher aussieht.
     */
    private void makeLeaves() {
        this.lChild = new Tree(null);
        this.rChild = new Tree(null);
    }

    /**
     * Fügt einen neuen Wert rekursiv in den Baum ein.
     * <p>
     * Falls ein Wert schon im Baum existiert, wird er nicht hinzugefügt. Das liegt daran, dass Binäre Bäume oft (auch
     * in diesem Blatt) als ein Basis für Mengen (Sets) und Wörterbücher (Maps/Dictionaries) benutzt werden, wo die
     * Einzigkeit der Werte wichtig ist.
     *
     * @param value - Wert, der eingefügt wird
     */
    public void insert(Integer value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }

        if (this.value == null) {
            this.value = value;
            makeLeaves();
            return;
        }

        if (value < this.value) {
            this.lChild.insert(value);
        } else if (value > this.value) {
            this.rChild.insert(value);
        }
    }

    /**
     * Fügt einen neuen Wert iterativ in den Baum ein.
     * <p>
     * Rekursive Implementierung ist einfacher zu verstehen und später zu warten. Allerdings benutzt Java selbst eine
     * iterative Implementierung für die {@link java.util.TreeMap}.
     *
     * @param value - Wert, der eingefügt wird
     */
    public void insertIterative(Integer value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }

        Tree whereTo = this;
        boolean inserted = false;

        while (!inserted) {
            if (whereTo.value == null) {
                whereTo.value = value;
                whereTo.makeLeaves();
                inserted = true;
            } else {
                if (value < whereTo.value) {
                    whereTo = whereTo.lChild;
                } else if (value > whereTo.value) {
                    whereTo = whereTo.rChild;
                } else {
                    inserted = true;
                }
            }
        }
    }

    /**
     * Berechnet die Höhe des Baumes
     *
     * @return die Höhe des Baumes
     */
    public int height() {
        if (this.value == null)
            return 0;

        return (Math.max(this.lChild.height(), this.rChild.height()) + 1);
    }

    /**
     * Prüƒt, ob der eingegebene Wert im Baum vorhanden ist
     *
     * @param value Wert, dessen Existenz geprüft wird
     * @return `true` falls Wert im Baum vorhanden ist, sonst `false`
     */
    public boolean exists(Integer value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }

        if (this.value == null)
            return false;

        if (value < this.value) {
            return this.lChild.exists(value);
        } else if (value > this.value) {
            return this.rChild.exists(value);
        } else {
            return true;
        }
    }

    /**
     * Findet den kleinsten Wert des Baumes
     *
     * @return der kleinste Wert im Baum
     */
    public Integer min() {
        if (this.lChild != null && this.lChild.value != null) {
            return lChild.min();
        }

        return this.value;
    }

    /**
     * Findet den größten Wert des Baumes
     *
     * @return der größte Wert im Baum
     */
    public Integer max() {
        if (this.rChild != null && this.rChild.value != null) {
            return rChild.max();
        }

        return this.value;
    }

    /**
     * Determiniert, ob der Baum entartet ist
     *
     * @return `true`, falls der Baum entartet ist, sonst `false`
     */
    public boolean isDegenerate() {
        if (this.value == null)
            return true;

        if (this.rChild.value != null && this.lChild.value == null) {
            return this.rChild.isDegenerate();
        }

        if (this.rChild.value == null && this.lChild.value != null) {
            return this.lChild.isDegenerate();
        }

        if (this.lChild.value == null && this.rChild.value == null) {
            return true;
        }

        return false;
    }

    /**
     * Berechnet den Wert der eingegebenen Prädikaten-Funktion für jeden Wert und gibt die Kumulative AND-Multiplikation
     * aus
     *
     * @param function Prädikaten-Funktion
     * @return `true`, falls die Funktion für alle Werte `true` ausgibt, sonst `false`
     */
    public boolean forAll(Function<Tree> function) {
        if (this.value == null)
            return true;

        return function.predicate(this) && this.lChild.forAll(function) && this.rChild.forAll(function);
    }

    /**
     * Gibt den Wert des Wurzels des Baumes aus
     *
     * @return der Wert des Wurzels
     */
    public Integer getRootValue() {
        return value;
    }

    /**
     * Gibt die String-Repräsentation des Baumes aus
     *
     * @return Liste aller Knoten in einer String-Repräsentation
     */
    @Override
    public String toString() {
        if (this.value == null) {
            return "";
        }

        return String.format("(%s %d %s)", this.lChild != null ? this.lChild.toString() : "", this.value,
                this.rChild != null ? this.rChild.toString() : "");
    }

    /**
     * Füllt eine Liste mit aller Werte des Baumes in der richtigen Reihenfolge
     * <p>
     * Benutzt In-Order-Traversal auf der Basis von BFS (Tiefensuche)
     *
     * @param list Liste, in den alle Werte des Baumes hinzugefügt werden
     */
    private void toList(List<Integer> list) {
        if (this.value == null) {
            return;
        }

        if (this.lChild != null) this.lChild.toList(list);
        list.add(this.value);
        if (this.rChild != null) this.rChild.toList(list);
    }

    /**
     * Gibt den Iterator für die Werte im Baum aus
     * @return Iterator über {@link Integer}
     */
    @Override
    public Iterator<Integer> iterator() {
        List<Integer> toIterate = new LinkedList<>();
        this.toList(toIterate);
        return toIterate.iterator();
    }
}