# prog1_ws1920

> Homework tasks of the "Programming 1" course at the Technische
> Universität Braunschweig, Germany.

## Licence

Copyright © 2019–2020 Nikita Karamov  
Licenced under the [BSD Zero Clause License](https://spdx.org/licenses/0BSD.html).
